import React, { FC } from 'react';

import { ListItemProtocol } from './listItem.types';
import { StyledWraper } from './listItem.styles';

export const ListItem: FC<ListItemProtocol> = ({
  title,
  details,
  color,
}: ListItemProtocol) => {
  return (
    <StyledWraper
      color={color}
    >{`${title} - ${details}`}</StyledWraper>
  );
};

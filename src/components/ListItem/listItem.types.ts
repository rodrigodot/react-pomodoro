export interface ListItemProtocol {
  title: string;
  color: string;
  details: string | number;
}

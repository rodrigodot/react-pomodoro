import styled from 'styled-components';

import { colors } from '../../theme/colors';

export const StyledWraper = styled.div`
  background-color: ${(props) => props.color};
  color: ${colors.fontColor};

  margin: 0.5em 0.3em;
  font-size: 1em;
  padding: 0.5em 1em;
  border: 2px solid ${colors.fontColor};
  border-radius: 0.3em;
  transition: background-color 4s;
`;

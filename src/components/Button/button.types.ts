import React from 'react';

export interface ButtonProtocol {
  title: string;
  onClick: CallableFunction;
  buttonProps?: React.ButtonHTMLAttributes<HTMLButtonElement>;
  buttonStyles?: React.CSSProperties;
}

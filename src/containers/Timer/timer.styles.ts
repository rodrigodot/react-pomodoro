import styled from 'styled-components';

import { colors } from '../../theme/colors';

export const StyledDisplay = styled.div`
  display: flex;
  flex: 12;
  color: ${colors.fontColor};
  background-color: ${(props) => props.color || 'green'};
  font-size: 7rem;
  justify-content: center;
  margin-bottom: 0.2em;
  transition: background-color 2s;
`;

export const StyledButtonGrouper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const StyledList = styled.div`
  margin-top: 1em;
`;

export const StyledListTitle = styled.h2`
  color: ${colors.fontColor};
  margin-bottom: 0.5em;
  text-align: center;
`;

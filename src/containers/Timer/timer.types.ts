export interface TimerProtocol {
  defaultTime: number;
  shortRestTime: number;
  longRestTime: number;
  cycles: number;
  onResume: () => void;
  onPause: () => void;
  onStop: () => void;
  onRest: () => void;
}

export interface ListItemProtocol {
  type: string;
  counter: number | string;
}

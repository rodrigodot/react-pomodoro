import React, { useState, FC, useEffect } from 'react';
import { useInterval } from '../../hooks/useInterval';

import { Button, ListItem } from '../../components';

import { fromSecondsToTime } from '../../utils';

import { TimerProtocol, ListItemProtocol } from './timer.types';
import {
  StyledDisplay,
  StyledButtonGrouper,
  StyledList,
  StyledListTitle,
} from './timer.styles';

import { colors } from '../../theme/colors';

const DEFAULT_INTERVAL = 1000;

export const Timer: FC<TimerProtocol> = (props: TimerProtocol) => {
  const {
    defaultTime,
    shortRestTime,
    longRestTime,
    cycles,
    onResume,
    onPause,
    onStop,
    onRest,
  } = props;

  const [list, setList] = useState<Array<ListItemProtocol>>([]);
  const [isRest, setIsRest] = useState(false);
  const [cyclesCounter, setCyclesCounter] = useState(0);
  const [bgColor, setBGColor] = useState(colors.onResume);
  const [time, setTime] = useState(defaultTime);
  const [isPaused, setIsPaused] = useState(false);
  const [interval, setInterval] = useState<number | null>(
    DEFAULT_INTERVAL
  );

  useInterval(() => {
    if (time === 0) return handleInterval();
    setTime(time - 1);
  }, interval);

  useEffect(() => {
    list.push({ type: 'working', counter: cyclesCounter });
  }, []);

  const handleInterval = () => {
    if (isRest) {
      setIsRest(false);
      resetTimer();
      return onResume();
    }

    const resInterval =
      cyclesCounter === cycles ? longRestTime : shortRestTime;
    const resetCounter =
      cyclesCounter === cycles ? 0 : cyclesCounter + 1;

    setIsRest(true);
    setCyclesCounter(resetCounter);
    onRestTime(resInterval);
    return onRest();
  };

  const onRestTime = (interval: number) => {
    setInterval(DEFAULT_INTERVAL);
    setTime(interval);
    setBGColor(colors.onRest);
    list.push({
      type: 'onRestTime',
      counter: `counter ${cyclesCounter} - interval ${interval}`,
    });
    onRest();
  };

  const resetTimer = () => {
    setTime(defaultTime);
    setInterval(DEFAULT_INTERVAL);
    setIsPaused(false);
    setBGColor(colors.onResume);
    list.push({ type: 'working', counter: cyclesCounter });
    onResume();
  };

  const stopTimer = () => {
    setInterval(null);
    onStop();
    setTime(0);
    list.push({ type: 'stopTimer', counter: cyclesCounter });
    setCyclesCounter(0);
    setBGColor(colors.onStop);
  };

  const pauseTimer = () => {
    if (time <= 0) return;
    const interval = isPaused ? DEFAULT_INTERVAL : null;
    const callBack = isPaused ? onResume : onPause;
    const color = isPaused ? colors.onResume : colors.onPause;

    setInterval(interval);
    setIsPaused(!isPaused);
    callBack();
    list.push({ type: 'pauseTimer', counter: cyclesCounter });
    setBGColor(color);
  };

  return (
    <div>
      <StyledDisplay color={bgColor}>
        {fromSecondsToTime(time)}
      </StyledDisplay>
      <StyledButtonGrouper>
        <Button
          title="Resetar"
          onClick={() => resetTimer()}
          buttonStyles={{
            color: colors.fontColor,
            borderColor: colors.fontColor,
            backgroundColor: bgColor,
            flex: 6,
          }}
        />
        <Button
          title={isPaused ? 'Continuar' : 'Pausar'}
          onClick={() => pauseTimer()}
          buttonStyles={{
            color: colors.fontColor,
            borderColor: colors.fontColor,
            backgroundColor: bgColor,
            flex: 6,
          }}
        />
      </StyledButtonGrouper>
      <StyledButtonGrouper style={{ marginTop: '0.5em' }}>
        <Button
          title="Parar"
          onClick={() => stopTimer()}
          buttonStyles={{
            color: colors.fontColor,
            borderColor: colors.fontColor,
            backgroundColor: bgColor,
            flex: 12,
          }}
        />
      </StyledButtonGrouper>

      <StyledList>
        <StyledListTitle>- Lista -</StyledListTitle>
        {!!list.length &&
          list.map((item, index) => {
            return (
              <ListItem
                color={bgColor}
                key={index}
                title={item.type}
                details={item.counter}
              />
            );
          })}
      </StyledList>
    </div>
  );
};

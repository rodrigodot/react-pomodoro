import styled from 'styled-components';

import { colors } from '../../theme/colors';

export const StyledContainer = styled.div`
  display: flex;
  padding: 1em;
  justify-content: center;
  min-height: 100vh;
  background-color: ${(props) =>
    props.color || colors.mainBackgroundColor};
  border-radius: 0.5em;
  border: ${`4px solid ${colors.fontColor}`};
  border-radius: 3em;
  transition: background-color 2s;
`;

export const StyledInnerContainer = styled.div`
  border: ${`4px solid ${colors.fontColor}`};
  background-color: ${(props) =>
    props.color || colors.innerBackgroundColor};
  border-radius: 0.5em;
  padding: 1em;
  padding-bottom: 3em;
  max-width: 500px;
  transition: background-color 2s;
`;

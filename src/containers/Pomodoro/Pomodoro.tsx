import React, { useState, FC } from 'react';

import { Timer } from '../../containers';

import {
  StyledContainer,
  StyledInnerContainer,
} from './pomodoro.styles';

import { colors } from '../../theme/colors';

export const Pomodoro: FC = () => {
  const [bgColor, setBGColor] = useState(colors.onResume);
  const [isTicking, setIsTiking] = useState(true);
  const onResume = () => {
    setIsTiking(true);
    setBGColor(colors.onResume);
  };
  const onPause = () => {
    setIsTiking(false);
    setBGColor(colors.onPause);
  };
  const onStop = () => {
    setIsTiking(false);
    setBGColor(colors.onStop);
  };
  const onRest = () => {
    setIsTiking(false);
    setBGColor(colors.onRest);
  };

  return (
    <StyledContainer>
      <StyledInnerContainer color={bgColor}>
        <Timer
          shortRestTime={10}
          longRestTime={15}
          defaultTime={5}
          cycles={4}
          onResume={() => onResume()}
          onPause={() => onPause()}
          onStop={() => onStop()}
          onRest={() => onRest()}
        />
      </StyledInnerContainer>
    </StyledContainer>
  );
};

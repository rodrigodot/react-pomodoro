export const colors = {
  mainBackgroundColor: '#ff77a9',
  innerBackgroundColor: '#008040',
  fontColor: '#fff',
  darFontColor: '#880e4f',
  onPause: '#ff7043',
  onResume: '#0277bd',
  onStop: '#f50057',
  onRest: '#43a047',
};

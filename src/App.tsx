import React, { FC } from 'react';

import { Pomodoro } from './containers';

const App: FC = () => {
  return (
    <div className="App">
      <Pomodoro />
    </div>
  );
};

export default App;
